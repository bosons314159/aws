#include <iostream>
#include <stdio.h>
#include <gmp.h>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>

using namespace boost;
using namespace std;

void print(vector<int> nv) {
	for (int i = 0; i < nv.size();++i) {
		cout << nv[i]<<", ";
	}
	cout << endl;
}

void print(int* v, int l) {
	for (int i = 0; i < l;++i) {
		cout << v[i]<<", ";
	}
	cout << endl;
}

char* gen_str(int x, int f = 0) {
       f = 0;
       std::string ss = "";
            if (f ==  1) {
               ss += "1";
            }
            for (int i = 0; i < x; ++i) {
                ss += "0";
            }
            if (f == 0) {
               ss += "1";
            }
        return strdup(ss.c_str());
}
            
char* refurbish(char* num, int f = 0) {
       int l = strlen(num);
       int cnt = 0;
       std::string cstr = "";
       while (cnt < l) {
           int nk = num[cnt]-'0';
           char* bs = gen_str(nk, f);
           cstr += bs;
           ++cnt;
       }
       return strdup(cstr.c_str());
}

int main(int argc, char* argv[]) {
	char* num = strdup(argv[1]);
        int l = strlen(num);
	cout << "Number entered was : " << num << endl;
        char* refurbished_num = refurbish(num);
        cout << refurbished_num << endl;
}
