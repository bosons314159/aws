#!/usr/local/bin/python

import sys
import gmpy2

def decode(num):
    prod = gmpy2.mpz(1)
    nz = num[::-1]
    sx = gmpy2.mpz(0)
    for x in nz:
       xk = int(x)
       term = gmpy2.mul(prod, xk)
       sx = gmpy2.add(sx, term)
       prod = gmpy2.mul(prod, gmpy2.mpz("4"))
    return str(sx) 
    
if __name__ == "__main__":
    num = str(sys.argv[1])
    d4num = decode(num)
    print(d4num)
