#!/usr/local/bin/python

import sys
import gmpy2

def convert(num, b):
  ss = ""
  nz = gmpy2.mpz(num)
  while nz > 0:
    rm = gmpy2.f_mod(nz, gmpy2.mpz(b))
    nz = gmpy2.div(nz, gmpy2.mpz(b))
    ss = ss + str(rm)
  ss = ss[::-1]
  return ss

if __name__ == "__main__":
   num = str(sys.argv[1])
   print("Number entered was: "+ str(num))
   bnum = convert(num, sys.argv[2])
   print(bnum)
