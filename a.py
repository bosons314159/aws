#!/usr/local/bin/python

from primes import primes
import sys

a=str(sys.argv[1])
for k in primes[:3000]:
    print(k, bin(int(a*k))[2:].count("0") % 10)
