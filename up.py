#!/usr/local/bin/python

import sys
import gmpy2

if __name__ == "__main__":
    num = str(sys.argv[1])
    num = num[::-1]
    cnt = 0
    l = len(num)
    prod = gmpy2.mpz("1")
    sx = gmpy2.mpz("0")
    while cnt < l:
      term = gmpy2.mul(prod, gmpy2.mpz(num[cnt]))
      sx = gmpy2.add(sx, term)
      prod = gmpy2.mul(prod, gmpy2.mpz("4"))
      cnt = cnt + 1
    print(str(sx))
