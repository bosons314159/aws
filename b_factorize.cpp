#include <iostream>
#include <stdio.h>
#include <gmp.h>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>

using namespace boost;
using namespace std;

void print(vector<int> nv) {
	for (int i = 0; i < nv.size();++i) {
		cout << nv[i]<<", ";
	}
	cout << endl;
}

void print(int* v, int l) {
	for (int i = 0; i < l;++i) {
		cout << v[i]<<", ";
	}
	cout << endl;
}

char* convertToBinary(char* num) {
      std::string ss = "";
      mpz_t nz;
      mpz_init(nz);
      mpz_set_str(nz, num , 10);
      mpz_t rm;
      mpz_init(rm);
      while (mpz_cmp_si(nz,0) > 0) {
          mpz_mod_ui(rm, nz, 2);
          ss= ss + strdup(mpz_get_str(0, 10, rm));
          mpz_div_ui(nz, nz, 2);
      }
      mpz_clear(nz);
      mpz_clear(rm);
      std::reverse(ss.begin(), ss.end());
      return strdup(ss.c_str());
}

int main(int argc, char* argv[]) {
	char* num = strdup(argv[1]);
	cout << "Number entered was : " << num << endl;
        char* bnum = convertToBinary(num);
        cout << "Binary Representation: \t"<< bnum << endl;
}
